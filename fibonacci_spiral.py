# Python application for drawing of squares related with Fibonacci sequence - using Turtle 

import turtle
from turtle import *

# MANUAL with commands:
# https://docs.python.org/3.3/library/turtle.html

window=turtle.Screen()
window.bgcolor("white")

i_previous=0
j_current=1
k_next=1

# The 'factor' makes possible to expand or shrink the scale of the print.
factor = 3

print("Fibonacci sequence follows a recursive relation:")
print("next element k = previous + current = i + j")
print("f(n+1) = f(n-1) + f(n)")
print("where n is the n-th term in the sequence.")

how_many = int(input('Enter the number of iterations (must be > 1): ')) 

for n in range(int(how_many)):

   red=255-30*n
   green=10*n
   blue=30*n
   if red < 0 or red > 255: red=20
   if green < 0 or green > 255: green=200
   if blue < 0 or blue > 255: blue=20
   turtle.colormode(255)
   tuple = (red, green, blue)
   turtle.pencolor(tuple)
   turtle.pensize(n)
   if n > 4: turtle.pensize(4)
   
   forward(factor*k_next)
   right(90)
 
   k_next=i_previous+j_current
   print("Step "+str(n+1)+". i="+str(i_previous)+", j="+str(j_current)+", k=Fib="+str(k_next))   
   i_previous=j_current
   j_current=k_next
   
waiting = input("Waiting. Press Enter key to finish...")
print(str(waiting))